﻿using NATS.Client;
using STAN.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SessionNotificationPlugin
{
    public class NATSDispatcher
    {

        private static int _sendIntervalMs = 300;
        private static IConnection _connection;
        static string _natsUrl = "nats://localhost:4222"; //this address should be provided in a config file and point to the NATS server in the cloud

        //Connect to NATS
        private static IConnection ConnectToNats() 
        {
            //Connection for NATS core
            ConnectionFactory factory = new ConnectionFactory();
            var options = ConnectionFactory.GetDefaultOptions();
            options.Url = _natsUrl;
            return factory.CreateConnection(options);
         
        }

        //private static IStanConnection ConnectToStan()
        //{
        //    //Connection for NATS streaming
        //    var cf = new StanConnectionFactory();
            

        //}

        ///<summary>
        ///Send a message to a subject.
        ///This uses NATS core.
        ///This option might lose messages and it only considers the last message as being important.
        ///</summary>

        public static void SendToSubject(string subjectName, string message)
        {
            using (_connection = ConnectToNats())
            {
                byte[] data = Encoding.UTF8.GetBytes(message);
                _connection.Publish(subjectName, data);
                Thread.Sleep(_sendIntervalMs);
            }
            
        }

        public static void SendToQueue(string subjectName, string message) 
        {
            var cf = new StanConnectionFactory();
            using (var c = cf.CreateConnection("test-cluster", "appname"))
            {
                using (c.Subscribe(subjectName, (obj, args) =>
                {
                    Console.WriteLine(
                       Encoding.UTF8.GetString(args.Message.Data));
                }))
                {
                    c.Publish(subjectName, Encoding.UTF8.GetBytes(message));
                }
            }


        }



    }
}

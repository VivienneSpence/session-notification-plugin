﻿using System;
using System.Threading;

namespace SessionNotificationPlugin
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Sending to NATS...");
            //NATSDispatcher.SendToSubject("testSubject", "Hello NATS");
            //Console.WriteLine("Message 1 sent to NATS.");
            //Thread.Sleep(2000);
            //NATSDispatcher.SendToSubject("testSubject", "Goodbye NATS");
            //Console.WriteLine("Message 2 sent to NATS.");
            //Console.ReadLine();

            Console.WriteLine("Sending to STAN...");
            NATSDispatcher.SendToQueue("testSubject", "Hello STAN");
            Console.WriteLine("Message 1 sent to STAN.");
            Thread.Sleep(2000);
            NATSDispatcher.SendToQueue("testSubject", "Goodbye STAN");
            Console.WriteLine("Message 2 sent to STAN.");
            Console.ReadLine();
        }
    }
}
